<?php
    include("includes/header.php");
    require_once("db/connexion.php");
    require_once("includes/functions.inc.php");
?>

<section class="content content-program text-center">
    <div class="container-fluid">
        
        <div class="program-head">  
            <div class="program-head-inner">
                <div class="small-title">
                    <a href="home.php" class="back text-left">
                        <i class="icon-arrow-right left"></i>
                    </a>
                    Incendies
                </div>
                <div class="small-subtitle lower">Gestionnaire de table</div>
            </div>
            
            <div class="row index-content">
                <div class="col-md-6">
                    <div class="wrap wrap-indexes">
                        <a href="#add" data-table="Incendie" class="single-btn hover-border go-btn">
                            <i class="icon-doc"></i><span>Ajouter un élément</span>
                        </a>

                        <form id="add" class="program-form" data-table="Incendie">
                            <div class="row">
                                <div class="col-sm-12 text-right fill-req" style="display:none;"><label>Veuillez remplir tous les champs</label></div>
                                <div class="col-sm-6"><label>Type Incendie</label><input type="text" name="typeInc"></div>
                                <div class="col-sm-6"><label>Type Equipement</label><input type="text" name="typeEquip" placeholder="ex: Casque"></div>
                                <div class="col-sm-6"><label>Debut</label><input type="text" name="debut" placeholder="ex: 05:30:00"></div>
                                <div class="col-sm-6"><label>Fin</label><input type="text" name="fin" placeholder="ex: 07:45:00"></div>
                                <div class="col-sm-6"><label>No Civique</label><input type="text" name="nocivique"></div>
                                <div class="col-sm-6"><label>Rue</label><input type="text" name="rue"></div>
                                <div class="col-sm-6"><label>Cartier</label><input type="text" name="cartier"></div>
                                <div class="col-sm-12 text-right"><input type="submit" class="submit" name="submit" value="Soumettre"></div>
                            </div>
                        </form>

                        <a href="#delete" data-table="Incendie" class="single-btn hover-border go-btn">
                            <i class="icon-close"></i><span>Supprimer un élément</span>
                        </a>

                        <form id="delete" class="program-form text-left" data-table="Incendie">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="styled-select">
                                        <?php echo displaySelectDelete($mysqli, $sql_bdd, 'Incendie'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-right"><input type="submit" class="submit" name="submit" value="Supprimer"></div>
                            </div>
                        </form>

                        <a href="#listall" data-table="Incendie" class="single-btn hover-border go-btn">
                            <i class="icon-list"></i><span>Lister les éléments</span>
                        </a>                  
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="box" class="textbox"></div>
                </div>
            </div>
        </div>
        
    </div>
</section>

<?php
  include("includes/footer.php");
?>
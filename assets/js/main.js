$(function(){

    // Retrait du 300ms de délai natif sur les browsers mobiles, app-feel
	FastClick.attach(document.body);

	// Ajax - Gestion clic bouton
	$('.wrap-indexes .single-btn').click(function(e){
		e.preventDefault();

		// Création des variables / contexte
		var id = $(this).attr('href');
		var finalID = id.substring(id.indexOf("#")+1);
		var tableName = $(this).data('table');
		// console.log(finalID + " table : " + tableName);

		if ( finalID == "listall" ){
			
			// Ajax - Appel
			$.ajax({
				type: 'post',
				data: { action: finalID, table: tableName },
			    success: function(data){
			  		var results = $($.parseHTML(data)).find('#results');
					$('#box').html(results);
			    },  
			    error: function(MLHttpRequest, textStatus, errorThrown){   
			        console.log('error : ' + errorThrown);
			    }
			});
		}

		if ( finalID == "add" || finalID == "delete" ){
			$(this).toggleClass('opened');
			$('#' + finalID).slideToggle();
		}
	});

	// Add form - Submit 
	$('#add').submit(function(e) {
		e.preventDefault();
		var isvalid = false;
		var tableName = $(this).data('table');
		// console.log(tableName);
		
		// Petite validation...
		$('#add .row > div').each(function(){
			// console.log('submit');

			if ( $(this).find('input[type="text"]').val() == '' ){
				$('#box').html('<pre>Veuillez remplir tous les champs.</pre>');
				isvalid = false;
			} else {
				isvalid = true;
			}

			if ( !isvalid ){ return false; } else { isvalid == true; }
		});

		if ( isvalid ){
			var params = $('#add').serialize();

			// Ajax - Appel
		    $.ajax({
	           type: 'post',
	           data: { action: "add", table: tableName, params: params },
	           success: function(data){
	               	// console.log(data);
	               	var results = $($.parseHTML(data)).find('#results');
					$('#box').html(results);
	           }
	        });
		}
	});

	// Delete form - Submit 
	$('#delete').submit(function(e) {
		e.preventDefault();
		var tableName = $(this).data('table');
		var selectedDelete = $('#selDelete').find(":selected").text();
		console.log(selectedDelete);

		// Ajax - Appel
	    $.ajax({
           type: 'post',
           data: { action: "delete", table: tableName, del: selectedDelete },
           success: function(data){
               	console.log(data);
               	var results = $($.parseHTML(data)).find('#results');
				$('#box').html(results);
           }
        });
	});
});

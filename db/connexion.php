<section id="status">
	<div class="container-fluid">
		<?php  
		require_once("config.php");

		// CONNEXION À LA BASE DE DONNÉES
		$mysqli = new mysqli($sql_server, $sql_user, $sql_passwd, $sql_bdd);

		$mysqli->query("SET NAMES 'utf8'"); 
		$mysqli->query("SET CHARACTER SET utf8");  
		$mysqli->query("SET SESSION collation_connection = 'utf8_unicode_ci'"); 

		if ($mysqli->connect_errno) {
		    echo "<p>Connexion impossible à la base de données <b>$sql_bdd</b> sur le serveur <b>$sql_server</b>.</p>";
		} else {
			echo '<p>Connexion bien établie.</p>';
		} 

		?>
	</div>
</section>
<?php
  include("includes/header.php");
?>

<section id="welcome" class="content text-center">
    <div class="container">
        <div class="small-title">Bienvenue sur le gestionnaire<br>de base de donnés des pompiers</div>
        <p class="small">Il s’agit de créer une base de données pour gérer l’ensemble des casernes de pompiers d’une grande ville (i.e. Montréal). La ville est divisée en plusieurs secteurs. Chaque caserne couvre un certain nombre de secteurs et chaque secteur est couvert par une caserne. Chaque caserne possède un certain nombre d’équipes de pompiers qui ont leur propre horaire. Chaque équipe est composée d’un certain nombre de pompiers. Les casernes possèdent aussi de l’équipement et des camions.</p>
        
        <p class="small">Lorsqu’un incendie se déclare, il faut déterminer quelle caserne et quelle équipe va répondre à l’appel. Il faut traiter le cas où un incendie majeur (ou des incendies multiples) oblige une caserne à demander de l’aide à d’autres casernes.</p>

        <p class="small">L'équipe de <b>feu</b><br>Louis Courchesne&nbsp;&nbsp;|&nbsp;&nbsp;Ghislain Durocher&nbsp;&nbsp;|&nbsp;&nbsp;Mathieu Koffi Kouadio&nbsp;&nbsp;|&nbsp;&nbsp;Cédric Vallée&nbsp;&nbsp;|&nbsp;&nbsp;Maxime Veilleux-Mavica</p>
        <a href="home.php" class="button">Entrer</a>
    </div>
</section>

<?php
  include("includes/footer.php");
?>
<?php
    include("includes/header.php");
    session_start();
    require_once("db/connexion.php");
?>

<section id="program" class="content text-center">
    <div class="container-fluid">
        <div class="small-title">Les pompiers</div>
        <div class="small-subtitle">Choisir une table</div>
        
        <div class="wrap-program">
            <a href="gestionnaire-incendies.php" class="single-program hover-border">
                <div class="title valign"><div><span><img src="/assets/img/icons/fire.png"></span></div>Incendies</div>
            </a>

            <a href="gestionnaire-employes.php" class="single-program hover-border">
                <div class="title valign"><div><span><img class="hat" src="/assets/img/icons/firehat.png"></span></div>Employés</div>
            </a>

            <a href="gestionnaire-casernes.php" class="single-program hover-border">
                <div class="title valign"><div><span><img src="/assets/img/icons/hydrant.png"></span></div>Casernes</div>
            </a>

            <a href="gestionnaire-secteurs.php" class="single-program hover-border">
                <div class="title valign"><div><span><img class="pin" src="/assets/img/icons/pin.png"></span></div>Secteurs</div>
            </a>
            
            <?php /* ?>
            <a href="gestionnaire-table.php" class="single-program hover-border">
                <div class="title valign">Réponses</div>
            </a>
            
            <a href="gestionnaire-table.php" class="single-program hover-border">
                <div class="title valign">Camions</div>
            </a>

            <a href="gestionnaire-table.php" class="single-program hover-border">
                <div class="title valign">Équipes</div>
            </a>

            <a href="gestionnaire-table.php" class="single-program hover-border">
                <div class="title valign">Équipement</div>
            </a>

            <a href="gestionnaire-table.php" class="single-program hover-border">
                <div class="title valign">Matériel</div>
            </a>

            <a href="gestionnaire-table.php" class="single-program hover-border">
                <div class="title valign">Entrepôts</div>
            </a>

            <a href="gestionnaire-table.php" class="single-program hover-border">
                <div class="title valign">Fournisseurs</div>
            </a>
            <?php */ ?>
        </div>
    </div>
</section>

<?php
  include("includes/footer.php");
?>
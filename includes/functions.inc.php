<?php
/*
FUNCTIONS
Regroupe les fonctions responsables de la gestion des événements AJAX
Ajout / Affichage du contenu des tables
*/

if(isset($_POST['action']) && !empty($_POST['action'])) {

    $action = $_POST['action'];
    $table = $_POST['table'];

    if ( $action == "add" ){
        $params = $_POST['params'];
    }

    if ( $action == "delete" ){
        $del = $_POST['del'];
    }

    switch($action) {
        case 'listall' : selectAll($mysqli, $sql_bdd, $table); break;
        case 'add' : addElement($mysqli, $sql_bdd, $table, $params); break; 
        case 'delete' : deleteElement($mysqli, $sql_bdd, $table, $del); break; 
    }
}

// Fonction générique pour SELECT
// ==============================
function selectAll($mysqli, $sql_bdd, $table){

    // Création du tableau contenant les résultats
    // ===========================================
    $tabSelectAll = array();
    mysqli_select_db($mysqli, $sql_bdd);

    // Requête et tableau des résultats
    // ================================
    $selection = "SELECT * FROM " . $table;
    $resultatSel = $mysqli->query($selection);

    if ( $resultatSel->num_rows > 0 ){
        while ($row = mysqli_fetch_array($resultatSel,MYSQLI_ASSOC)){
            $tabSelectAll[] = $row;
        }
        mysqli_free_result($resultatSel);

        echo '<div id="results"><pre>';
        print_r($tabSelectAll);
        echo '</pre></div>';
    } else {
        echo '<div id="results"><pre>';
        echo "Aucun résultat.";
        echo '</pre></div>';
    }
    mysqli_close($mysqli);
}

// Fonction générique pour INSERT
// ==============================
function addElement($mysqli, $sql_bdd, $table, $params){

    $values = array();
    parse_str($params, $values);
    print_r($values);

    // Création de la chaîne d'insertion
    // =================================
    $insertString = "";
    $nb = 1;
    foreach ( $values as $value ){
        if ( $nb == count($values) ){
            $insertString .= "'$value'"; 
        } else {
            $insertString .= "'$value',"; 
        }
        $nb++;
    }

    // Requête d'insertion dynamique
    // =============================
    $insertion = "INSERT INTO " .$table. " values ('',".$insertString.")";
    $resultatInsert = $mysqli->query($insertion);

    if ( $mysqli->affected_rows > 0 ){
        echo '<div id="results"><pre>';
        echo 'Élément bien ajouté à la table.';
        echo '</pre></div>';
    } else {
        echo '<div id="results"><pre>';
        echo "Insertion impossible.";
        echo '</pre></div>';
    }
    mysqli_free_result($resultatInsert);
    mysqli_close($mysqli);
}

// Fonction pour générer le contenu du SELECT
// ==========================================
function displaySelectDelete($mysqli, $sql_bdd, $tab){

    // Création du tableau contenant les résultats
    // ===========================================
    $tabSelectAll = array();
    mysqli_select_db($mysqli, $sql_bdd);

    // Requête et tableau des résultats
    // ================================
    $selection = "SELECT * FROM " . $tab;
    $resultatSel = $mysqli->query($selection);

    if ( $resultatSel->num_rows > 0 ){
        while ($row = mysqli_fetch_array($resultatSel,MYSQLI_ASSOC)){
            $tabSelectAll[] = $row;
        }
        mysqli_free_result($resultatSel);
        mysqli_close($mysqli);

        // Création des options du SELECT
        // ==============================
        $selectDisplay = "<select id='selDelete'>";
        $selectDisplay .= "<option selected='true' disabled='disabled'>Choisir le ID de l'élément</option>";
        foreach ( $tabSelectAll as $tabSelect ){
            $first_value = reset($tabSelect);
            $selectDisplay .= "<option>".$first_value."</option>";
        }
        $selectDisplay .= "</select>";

        return $selectDisplay;
    }
}

// Fonction générique pour INSERT
// ==============================
function deleteElement($mysqli, $sql_bdd, $table, $del){

    // Choisir la BD
    // =============
    mysqli_select_db($mysqli, $sql_bdd);

    // Requête de suppression dynamique
    // ================================
    $suppression = "DELETE FROM " .$table. " where No".$table." = ".$del."";
    $resultatSupp = $mysqli->query($suppression);

    if ( $mysqli->affected_rows > 0 ){
        echo '<div id="results"><pre>';
        echo 'Élément bien supprimé de la table.';
        echo '</pre></div>';
    } else {
        echo '<div id="results"><pre>';
        echo "Suppression impossible.<br><br>L'élément a peut-être déjà été supprimé, ou peut être lié à une autre table.";
        echo '</pre></div>';
    }
    mysqli_free_result($resultatSupp);
    mysqli_close($mysqli);
}

?>
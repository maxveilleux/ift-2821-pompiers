<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Les Pompiers | Présenté par Maxime Veilleux-Mavica, Louis Courchesne, Ghislain Durocher, Mathieu Koffi Kouadio et Cédric Vallée.">
    <meta name="author" content="Les Pompiers">
    <link rel="shortcut icon" href="/assets/img/favicon.png">

    <title>IFT 2821 | Les Pompiers</title>

    <!-- Custom styles -->
    <link href="assets/css/app.css?v1" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

<body>
      
    <div id="wrap">
        <header class="navbar navbar-inverse" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="/">
                <div class="brand-icon"><img src="/assets/img/icons/firehat.png"></div>
                <div class="brand-text">
                    <span>Les Pompiers</span>
                    <span>Gestionnaire de base de données</span>
                </div>
              </a>
            </div>
          </div>
        </header>

        <div id="site-main">